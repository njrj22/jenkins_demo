package co.pragra.demo.java.demo.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

public class IndexController {

    @GetMapping("/")
    public String getHomePage(Model model){
        return "index";
    }
}
